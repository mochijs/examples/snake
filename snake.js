define(['../engine/Mochi', './food'], function(Mochi, Food) {

    /** Wrapper class for instanceof */
    class BodyPiece extends Mochi.Entity {}

    /** Creates body pieces for the snake factory */
    class BodyPieceFactory {
        constructor() {
            this.bodyAsset = new Mochi.Content.Asset("assets/ice_cream_stretched_green.png");
        }

        load() {
            return this.bodyAsset.load();
        }

        make(grid, x, y) {
            let bodyPiece = new BodyPiece(x, y);
            bodyPiece.add(new Mochi.Graphics.Sprite(this.bodyAsset, {
                width: 1,
                height: 1
            }));
            bodyPiece.add(new Mochi.Physics.Body(new Mochi.Physics.Shape.Point()));
            bodyPiece.init();
            return bodyPiece;
        }
    }

    /**
     * Creates snakes that can be used to control the game.
     */
    class SnakeFactory {
        constructor() {
            this.headAsset = new Mochi.Content.Asset("assets/ice_creamcat_template.png");
            this.bodyPieceFactory = new BodyPieceFactory();
        }

        /** Returns promise for when all this object's assets are done loading */
        load() {
            return new Promise((resolve, reject) => {
                this.headAsset.load().then(function() {
                    this.bodyPieceFactory.load().then(function() {
                        resolve();
                    });
                }.bind(this));
            });
        }

        makeDefaultSnake(grid) {
            return this.make(grid, 0, 0, 3);
        }

        make(grid, x, y, length) {
            let snake = new Snake(grid, x, y, length, this.bodyPieceFactory);
            // Add the head of the snake component.
            snake.add(new Mochi.Graphics.Sprite(this.headAsset, {
                width: 1,
                height: 1
            }));
            // Add a body component.
            snake.add(new Mochi.Physics.Body(new Mochi.Physics.Shape.Point()));
            snake.init();
            return snake;
        }
    }

    /**
     * The snake class is used to maintain the entity that roams about the map.
     */
    class Snake extends Mochi.Entity {
        constructor(grid, x, y, length, bodyFactory) {
            super(x, y);
            this.grid = grid;
            this.bodyFactory = bodyFactory;
            this.length = length; // Starting length
            this.growth = 1;
            this.bodyPieces = [];
            // By default, we are traveling right.
            this.direction = Snake.Directions.Right;
            this.timeSinceMoved = 0;
            // call the collide function when we collide.
            this.events.subscribe("collision", this.collide.bind(this));
        }

        /** Initializes the input bindings for the snake. */
        bind(input) {
            input.onKeyDown(this.get('snakeControl').up, this.goUp.bind(this));
            input.onKeyDown(this.get('snakeControl').down, this.goDown.bind(this));
            input.onKeyDown(this.get('snakeControl').left, this.goLeft.bind(this));
            input.onKeyDown(this.get('snakeControl').right, this.goRight.bind(this));
        }

        /**
         * Updates the snake's position based on its velocity.
         * @param ms
         */
        update(ms) {
            this.timeSinceMoved += ms;
            if ((this.timeSinceMoved / 1000) >= (1/Snake.UNITS_PER_SECOND)) {
                this.timeSinceMoved -= (1/Snake.UNITS_PER_SECOND) * 1000;
                this._move();
            }
            while (this.bodyPieces.length > this.length) {
                this.grid.removeEntity(this.bodyPieces[0]);
                this.bodyPieces.splice(0, 1);
            }
            if (this.x < 0 || this.y < 0 || this.x >= this.grid.size || this.y >= this.grid.size) {
                this.grid.removeEntity(this);
                for (let i = 0; i < this.bodyPieces.length; i++) {
                    this.grid.removeEntity(this.bodyPieces[i]);
                }
                if (this.grid.entities.find(function(i) { return i instanceof Snake}) == null){
                    this.grid.endGame();
                }
            }
        }

        collide (entity) {
            if (entity instanceof Food.Food) {
                this.length += entity.rotten ? this.growth * -1 : this.growth;
                this.length = Math.max(0, this.length);
                entity.reset(this.grid, this.grid.size);
                this.grid.eatFood();
            } else if (entity instanceof BodyPiece) {
                this.grid.removeEntity(this);
                // TODO: OH GOD THIS FEELS AWFUL TO WRITE
                let otherSnake = this.grid.entities.find(function(i) { return i instanceof Snake});
                if (otherSnake != null) {
                    this.grid.collisionManager.collideOne(otherSnake);
                }
                // TODO: Fix the above please, please, please, please god, please
                for (let i = 0; i < this.bodyPieces.length; i++) {
                    this.grid.removeEntity(this.bodyPieces[i]);
                }
                if (otherSnake == null) {
                    this.grid.endGame();
                }
            } else if (entity instanceof Snake) {
                this.grid.endGame();
            }
            while (this.bodyPieces.length > this.length) {
                this.grid.removeEntity(this.bodyPieces[0]);
                this.bodyPieces.splice(0, 1);
            }
        }

        goUp() {
            if (this.direction == Snake.Directions.Down) return;
            this.events.broadcast("direction", Snake.Directions.Up);
            this.direction = Snake.Directions.Up;
        }
        goDown() {
            if (this.direction == Snake.Directions.Up) return;
            this.events.broadcast("direction", Snake.Directions.Down);
            this.direction = Snake.Directions.Down;
        }
        goLeft() {
            if (this.direction == Snake.Directions.Right) return;
            this.events.broadcast("direction", Snake.Directions.Left);
            this.direction = Snake.Directions.Left;
        }
        goRight() {
            if (this.direction == Snake.Directions.Left) return;
            this.events.broadcast("direction", Snake.Directions.Right);
            this.direction = Snake.Directions.Right;
        }
        goDirection(direction) {
            this.direction = direction;
        }


        /**
         * Tells the snake to move a unit in its current direction.
         * @private
         */
        _move() {
            let bodyPiece = this.bodyFactory.make(this.grid, this.x, this.y);
            this.bodyPieces.push(bodyPiece);
            this.grid.addEntity(bodyPiece);
            switch(this.direction) {
                case Snake.Directions.Up:
                    this.y -= 1;
                    break;
                case Snake.Directions.Down:
                    this.y += 1;
                    break;
                case Snake.Directions.Left:
                    this.x -= 1;
                    break;
                case Snake.Directions.Right:
                    this.x += 1;
                    break;
            }
        }
    }
    /** Enum for the direction the snake is traveling */
    Snake.Directions = {
        Up: 0,
        Down: 1,
        Left: 2,
        Right: 3
    };
    Snake.UNITS_PER_SECOND = 4;

    return {
        Snake: Snake,
        Factory: SnakeFactory
    };
});