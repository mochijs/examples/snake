# Team:
 1. Kenny Jones (kgjones3)
 2. Justin Patzer (jepatzer)
 3. Robby Seligson (rfseligs)

# For the Professor:

The server is named server.js, it can be run using node JS:
```
node server.js
```

You can connect to it via localhost on port 3000.

Only one client can be active on a server until they select 2 player multiplayer, then it is opened up to an additional.

# Controls

## Local Multiplayer:

wasd - control snake 1
ijkl - control snake 2

## Networked:

wasd - control your snake

The second player to join (select 2-player) will be the lower snake.

Each snake has personalized scores/food to make the game cooperative (see how long both can live). The game ends when
both players die.

# Setup (if needed)

If there are no packages installed, install them using npm:
```
npm install
```
In the current directory with package.json in it.

# Checking out project
This project can be easily cloned in a branch:

```git
git clone -b {yourbranch} git@gitlab.com:csc481/snake.git
```

It'll pull the game engine in.

## Pulling from local repo.
Make sure to also keep an updated copy of the engine alongside the code
```bash
git pull
cd ../engine
git pull
```
