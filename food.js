define(['../engine/Mochi'], function (Mochi) {

    /** Component that adds rotting behavior. Requires sprite. */
    class RottingComponent extends Mochi.Component {
        /** Assets necessary to perform actions */
        constructor(normal, rotting) {
            super();
            this.normal = normal;
            this.rotting = rotting;
        }

        init(entity) {
            entity.events.subscribe("decay", this._decay.bind(this));
            entity.events.subscribe("reset", this._reset.bind(this));
        }

        _decay(entity) {
            entity.sprite.image = this.rotting;
        }

        _reset(entity) {
            entity.sprite.image = this.normal;
        }
    }
    RottingComponent.Name = "rotting";

    class FoodFactory {
        constructor() {
            // Create assets for all the different images required.
            this.foodAsset = new Mochi.Content.Asset("assets/snowflake.png");
            this.foodRotten = new Mochi.Content.Asset("assets/sun.png");
        }

        load() {
            return new Promise((resolve, reject) => {
                this.foodAsset.load().then(function() {
                    this.foodRotten.load().then(function() {
                        resolve();
                    });
                }.bind(this));
            });
        }

        make(grid, x, y) {
            let food = new Food(x, y);
            // Add the initial appearance, not rotted.
            food.add(
                new Mochi.Graphics.Sprite(this.foodAsset, {
                    width: 1,
                    height: 1
                })
            );
            // Add a component for rotting.
            food.add(
                new RottingComponent(this.foodAsset, this.foodRotten)
            );
            // Lastly, adds a body for collision.
            food.add(new Mochi.Physics.Body(new Mochi.Physics.Shape.Point()));
            food.init();
            return food;
        }
    }

    /**
     * A food object for the snake to eat.
     */
    class Food extends Mochi.Entity {
        /**
         * Defines the food object.
         */
        constructor(x ,y) {
            super(x, y);
            this.time = 0;
            this.rotten = false;
        }

        /**
         * A function for turning regular food into rotten food.
         */
        decay(){
            this.rotten = true;
            this.events.broadcast("decay", this);
        }
        /**
         * Updates how the object works.
         */
        update(ms){
            if ( !this.rotten && this.time > 6000){
                this.decay();
            }
            this.time += ms;

        }

        reset(grid, cells){
            this.x = Math.round(this.getRandomArbitrary(0, cells - 1));
            this.y = Math.round(this.getRandomArbitrary(0, cells - 1));
            this.time = 0;
            this.rotten = false;
            this.events.broadcast("reset", this);
            while (grid.collisionManager.collideOne(this)) {
                this.x = Math.round(this.getRandomArbitrary(0, cells - 1));
                this.y =  Math.round(this.getRandomArbitrary(0, cells - 1));
            }
        }
        /**
         * Generates a random number between two values.
         * @param min The minimum value.
         * @param max The maximum value.
         * @returns {*} The random number.
         */
        getRandomArbitrary(min, max) {
            return Math.random() * (max - min) + min;
        }


    }

    /**
     * Returns the food object.
     */
    return {
        Food: Food,
        Factory: FoodFactory
    };

});