define(['../engine/Mochi'], function(Mochi) {

    class SnakeControl extends Mochi.Component {


        constructor(input, data) {
            super();
            this.input = input;
            this.up = data.up;
            this.down = data.down;
            this.left = data.left;
            this.right = data.right;


        }

        init(entity){
            this.entity = entity;
        }



        // The name of the ship component within mochi
    }
    SnakeControl.Name = "snakeControl";

    return SnakeControl;
});
