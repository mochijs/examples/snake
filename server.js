// Get the Mochi Server version.
var Mochi = require('../engine/MochiServer');

// Creating a Mochi game server on port 3000
let server = new Mochi.Server.GameServer(3000);
server.setupWebServer();
var app = server.app;
var path = server.path;
var http = server.http;
var express = server.express;
var io = server.io;

// Give main client page.
app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});
// expose the engine
app.use('/engine/', express.static(path.join(__dirname, '..', 'engine')));
// expose our game files
app.use('/', express.static(__dirname));


var clients = 0;
io.on('connection', function(socket) {
    clients++;
    console.log("Number clients: " + clients);

    // give response based on number of clients
    if (clients == 2) {
        // tell all clients, assign which is which
        socket.emit('client_ready_2');
        socket.broadcast.emit('client_ready_1');
    } else if (clients == 1) {
        // nothing for now
    } else {
        // tell the client that a game is already going on, close em
        socket.emit('clients_full');
        clients--;
        console.log("Clients full, rejecting: " + clients);
        socket.disconnect('full');
    }

    // forward events on snake direction
    socket.on('snake_direction', function(data) {
        console.log("Receiving snake direction... " + data);
        socket.broadcast.emit('snake_direction', data);
    });

    socket.on('snake_end', function(data) {
        console.log("One of the snakes lost...");
        socket.broadcast.emit('snake_end');
    });

    socket.on('disconnect', function () {
        clients--;
        console.log("Number clients: " + clients);
    });
});

// expose the web server
server.listen();